<?php
require('config.php');
require('class.game.php');
require('class.player.php');

?>
<!DOCTYPE html>
<html>
<head>
   <title></title>
</head>
<body>
<?php if( !isset($_GET['pid']) || empty($_GET['pid'])): ?> 
   <h2>No player defined</h2>
<?php else: ?>
   <?php
   $player = $pdo->prepare('select rowid, * from players where rowid=:playerid');
   $player->execute(array(
         ':playerid' => $_GET['pid']
      ));
   $player = $player->fetchAll(PDO::FETCH_CLASS, 'player')[0];
   ?>
   Player Record: <?php echo $player->score; ?>
   <ul>
   <?php foreach($player->allGames as $game): ?>
      <li><?php echo $game->player1['name']?> (<?php echo $game->p1score; ?>) v. <?php echo $game->player2['name']; ?> (<?php echo $game->p2score; ?>)</li>
   <?php endforeach; ?>
   </ul>
<?php endif; ?>
</body>
</html>