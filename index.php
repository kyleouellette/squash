<?php

require('config.php');
require('class.game.php');
require('class.player.php');

$games = $pdo->query('select games.rowid as gameid, * from games')->fetchAll(PDO::FETCH_CLASS, 'game');

?>
<!DOCTYPE html>
<html>
<head>
   <title>Squash // Squanch</title>
   <link rel="stylesheet" type="text/css" href="_css/main.css" />
</head>
<body>
<?php foreach($games as $game): ?>
   <table cellspacing=0 cellpadding=0>
      <tr class="players">
         <td><a href="player.php?pid=<?php echo $game->p1id; ?>"><?php echo $game->player1['name']?></a></td>
         <td><a href="player.php?pid=<?php echo $game->p2id;?>"><?php echo $game->player2['name']?></a></td>
      </tr>
      <tr class="scores">
         <td><?php echo $game->p1score;?></td>
         <td><?php echo $game->p2score; ?></td>
      </tr>
   </table>
<?php endforeach; ?>
<?php new Player();?>
</body>
</html>