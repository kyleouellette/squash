<?php

/**
 * Basic configuration file.
 */

$pdo = new PDO('sqlite:data/.data.db');

/**
 * Functions
 */
function debug($what){
   echo '<pre>';
   print_r($what);
   echo '</pre>';
}