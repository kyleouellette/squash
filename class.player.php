<?php

class Player{
   public $rowid = 1;
   public $score = 0;

   function __construct(){
      global $pdo;

      $getGames = $pdo->prepare('select rowid as gameid, * from games where p1id=? or p2id=?');
      $getGames->execute(array(
            $this->rowid,
            $this->rowid
         ));
      $this->allGames = $getGames->fetchAll(PDO::FETCH_CLASS, 'game');
      if(count($this->allGames) > 0){
         $this->makeScore();
      }
      return $this;
   }

   function makeScore(){
      foreach ($this->allGames as $game) {
         if( $game->p1id == $this->rowid ){
            $this->score += $game->p1score;
         }else{
            $this->score += $game->p2score;
         }
      }
   }
}